{
  description = "Deployment for skynet";

  inputs = {
    # gonna start off with a fairly modern base
    nixpkgs.url = "nixpkgs/nixos-unstable";
    # Return to using unstable once the current master is merged in
    # nixpkgs.url = "nixpkgs/nixos-unstable";

    # utility stuff
    flake-utils.url = "github:numtide/flake-utils";
    agenix.url = "github:ryantm/agenix";
    arion.url = "github:hercules-ci/arion";
    alejandra = {
      url = "github:kamadorueda/alejandra/3.0.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    colmena.url = "github:zhaofengli/colmena";
    attic.url = github:zhaofengli/attic;

    # email
    # simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver";
    simple-nixos-mailserver = {
      inputs.nixpkgs.follows = "nixpkgs";
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "misc%2Fnixos-mailserver";
    };

    # account.skynet.ie
    skynet_ldap_backend = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "ldap%2Fbackend";
    };
    skynet_ldap_frontend = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "ldap%2Ffrontend";
    };
    skynet_website_renew = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2Falumni-renew";
    };
    skynet_website_games = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2Fgames.skynet.ie";
    };
    skynet_discord_bot = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "discord-bot";
    };
    compsoc_public = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fcompsoc";
      repo = "presentations%2Fpresentations";
    };

    # skynet.ie
    skynet_website = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2017";
    };
    skynet_website_2023 = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2017";
      rev = "c4d61c753292bf73ed41b47b1607cfc92a82a191";
    };
    skynet_website_2017 = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2017";
    };
    skynet_website_2009 = {
      type = "gitlab";
      host = "gitlab.skynet.ie";
      owner = "compsoc1%2Fskynet";
      repo = "website%2F2009";
    };
  };

  nixConfig = {
    bash-prompt-suffix = "[Skynet Dev] ";
    extra-substituters = "https://nix-cache.skynet.ie/skynet-cache";
    extra-trusted-public-keys = "skynet-cache:zMFLzcRZPhUpjXUy8SF8Cf7KGAZwo98SKrzeXvdWABo=";
  };

  outputs = {
    self,
    nixpkgs,
    agenix,
    alejandra,
    colmena,
    ...
  } @ inputs: let
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    formatter.x86_64-linux = alejandra.defaultPackage."x86_64-linux";

    devShells.x86_64-linux.default = pkgs.mkShell {
      name = "Skynet build env";
      nativeBuildInputs = [
        pkgs.buildPackages.git
        colmena.defaultPackage."x86_64-linux"
        pkgs.attic-client
        pkgs.buildPackages.nmap
      ];
      buildInputs = [agenix.packages.x86_64-linux.default];
      shellHook = ''export EDITOR="${pkgs.nano}/bin/nano --nonewlines"; unset LD_LIBRARY_PATH;'';
    };

    colmena = {
      meta = {
        nixpkgs = import nixpkgs {
          system = "x86_64-linux";
          overlays = [];
        };
        specialArgs = {
          inherit inputs self;
        };
      };

      # installed for each machine
      defaults = import ./machines/_base.nix;

      # firewall machiene
      agentjones = import ./machines/agentjones.nix;

      # ns1
      vendetta = import ./machines/vendetta.nix;

      # ns2
      vigil = import ./machines/vigil.nix;

      # icecast - ULFM
      galatea = import ./machines/galatea.nix;

      # LDAP host
      kitt = import ./machines/kitt.nix;

      # Gitlab
      glados = import ./machines/glados.nix;

      # Gitlab runners
      wheatly = import ./machines/wheatly.nix;

      # email
      gir = import ./machines/gir.nix;

      # backup 1
      neuromancer = import ./machines/neuromancer.nix;

      # Skynet, user ssh access
      skynet = import ./machines/skynet.nix;

      # Main skynet sites
      earth = import ./machines/earth.nix;

      # Nextcloud
      cadie = import ./machines/cadie.nix;

      # trainee server
      marvin = import ./machines/marvin.nix;

      # Public Services
      calculon = import ./machines/calculon.nix;

      # metrics
      ariia = import ./machines/ariia.nix;
    };
  };
}
