{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  name = "nextcloud";
  cfg = config.services.skynet."${name}";
  domain = "${cfg.domain.sub}.${cfg.domain.base}.${cfg.domain.tld}";
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Nextcloud";

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = name;
      };
    };
  };

  config = mkIf cfg.enable {
    # shove the entire config file into secrets

    age.secrets.nextcloud_admin_pass = {
      file = ../secrets/nextcloud/pw.age;
      owner = "nextcloud";
      group = "nextcloud";
    };

    services.skynet.acme.domains = [
      domain
      "onlyoffice.${domain}"
    ];

    services.skynet.dns.records = [
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
      {
        record = "onlyoffice.${cfg.domain.sub}";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    # /var/lib/nextcloud/data

    services.nextcloud = {
      enable = true;
      package = pkgs.nextcloud28;
      hostName = domain;
      https = true;

      configureRedis = true;

      database.createLocally = true;
      config = {
        dbtype = "pgsql";
        adminpassFile = config.age.secrets.nextcloud_admin_pass.path;
      };

      appstoreEnable = true;

      extraApps = with config.services.nextcloud.package.packages.apps; {
        inherit forms groupfolders maps notes onlyoffice polls;
      };

      settings = {
        trusted_proxies = ["193.1.99.65"];
        default_phone_region = "IE";
        mail_smtpmode = "sendmail";
        mail_sendmailmode = "pipe";
      };
    };

    nixpkgs.config.allowUnfree = true;
    services.onlyoffice = {
      enable = true;
    };

    services.nginx.virtualHosts = {
      ${domain} = {
        forceSSL = true;
        useACMEHost = "skynet";
      };
      "onlyoffice.${domain}" = {
        forceSSL = true;
        useACMEHost = "skynet";
        locations."/".proxyPass = "http://127.0.0.1:8000";
      };
    };
  };
}
