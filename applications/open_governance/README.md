# Open Governance

Started by DCU this is an initiative to make the running of (computer) societies more open and resilient.  
The goal is to back these up in multiple locations.


| Uni | Tag      | Repo                                                     | Notes |
|-----|----------|----------------------------------------------------------|-------|
| DCU | redbrick | https://github.com/redbrick/open-governance              |       |
| UL  | skynet   | https://gitlab.skynet.ie/compsoc1/compsoc/open-goverance |       |
|     |          |                                                          |       |


## Keys
We host our own keyserver: https://keyserver.skynet.ie  
Use it in commands like so: 
``gpg --keyserver hkp://keyserver.skynet.ie:80 --send-key KEY_ID``
