/*
This file is for hosting teh open governance for other societies
*/
{
  lib,
  config,
  pkgs,
  ...
}:
with lib; let
  # - instead of _ for dns reasons
  name = "open-governance";

  cfg = config.services.skynet."${name}";
  folder = "/var/skynet/${name}";
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Open Governance";
  };

  config = {
    services.skynet.acme.domains = [
      "${name}.skynet.ie"
    ];

    services.skynet.dns.records = [
      {
        record = "${name}";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    # create a folder to store the archives
    systemd.tmpfiles.rules = [
      "d  ${folder}            0755 ${config.services.nginx.user} ${config.services.nginx.group}"
      "L+ ${folder}/README.md  -    -    -     -   ${./README.md}"
    ];

    services.nginx.virtualHosts = {
      "${name}.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        root = folder;
        locations = {
          "/".extraConfig = "autoindex on;";

          # show md files as plain text
          "~ \.md".extraConfig = ''
            types {
               text/plain md;
            }
          '';
        };
      };
    };
  };
}
