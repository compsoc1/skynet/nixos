/*
This file is for hosting teh open governance for other societies
*/
{
  lib,
  config,
  pkgs,
  ...
}:
with lib; let
  name = "keyserver";
  cfg = config.services.skynet."${name}";
  port = 11371;
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Public Keyserver";
  };

  config = mkIf cfg.enable {
    services.skynet.acme.domains = [
      "${name}.skynet.ie"
    ];

    services.skynet.dns.records = [
      {
        record = "${name}";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.hockeypuck = {
      enable = true;
      port = port;
    };

    # hockeypuck needs a database backend
    services.postgresql = {
      enable = true;
      ensureDatabases = ["hockeypuck"];
      ensureUsers = [
        {
          name = "hockeypuck";
          ensureDBOwnership = true;
        }
      ];
    };

    services.nginx.virtualHosts = {
      "${name}.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        locations."/" = {
          proxyPass = "http://localhost:${toString port}";
        };
      };
    };
  };
}
