{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  name = "discord_bot";
  cfg = config.services.skynet."${name}";
in {
  imports = [
    inputs.skynet_discord_bot.nixosModule."x86_64-linux"
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet LDAP backend server";
  };

  config = mkIf cfg.enable {
    #backups = [ "/etc/silver_ul_ical/database.db" ];

    age.secrets.discord_token.file = ../secrets/discord/token.age;
    age.secrets.discord_ldap.file = ../secrets/discord/ldap.age;
    age.secrets.discord_mail.file = ../secrets/email/details.age;
    age.secrets.discord_wolves.file = ../secrets/wolves/details.age;

    # this is what was imported
    services.skynet_discord_bot = {
      enable = true;

      env = {
        discord = config.age.secrets.discord_token.path;
        ldap = config.age.secrets.discord_ldap.path;
        mail = config.age.secrets.discord_mail.path;
        wolves = config.age.secrets.discord_wolves.path;
      };

      discord.server = "689189992417067052";
    };
  };
}
