{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  name = "website_users";
  cfg = config.services.skynet."${name}";
  php_pool = name;
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet User Linux Server";
  };

  config = {
    # we havea  more limited ports range on the skynet server
    services.skynet.prometheus.ports = {
      node = 9000;
    };

    # allow more than admins access
    services.skynet.ldap_client = {
      groups = [
        "skynet-admins-linux"
        "skynet-users-linux"
      ];
    };

    # Website config
    services.skynet.acme.domains = [
      "users.skynet.ie"
      "*.users.skynet.ie"
    ];

    services.skynet.dns.records = [
      {
        record = "users";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
      {
        record = "*.users";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    environment.systemPackages = with pkgs; [
      vim
      php
    ];

    networking = {
      defaultGateway = {
        address = lib.mkDefault "193.1.96.161";
        interface = lib.mkDefault "eth1";
      };
    };

    # normally services cannot read home dirs
    systemd.services.nginx.serviceConfig.ProtectHome = "read-only";
    systemd.services."phpfpm-${php_pool}".serviceConfig.ProtectHome = lib.mkForce "read-only";

    services.phpfpm.pools.${php_pool} = {
      user = config.services.nginx.user;
      group = config.services.nginx.group;
      settings = {
        "listen.owner" = config.services.nginx.user;
        "pm" = "dynamic";
        "pm.max_children" = 32;
        "pm.max_requests" = 500;
        "pm.start_servers" = 2;
        "pm.min_spare_servers" = 2;
        "pm.max_spare_servers" = 5;
        "php_admin_value[error_log]" = "stderr";
        "php_admin_flag[log_errors]" = true;
        "catch_workers_output" = true;
      };
      phpEnv."PATH" = lib.makeBinPath [pkgs.php];
    };

    services.nginx.virtualHosts = {
      # main site
      "*.users.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        serverName = "~^(?<user>.+)\.users\.skynet\.ie";

        # username.users.skynet.ie/
        # user goes:
        #   chmod 711 ~
        #   chmod -R 755 ~/public_html

        locations = {
          "/" = {
            alias = "/home/$user/public_html/";
            index = "index.html";
            extraConfig = ''
              autoindex on;
            '';
            tryFiles = "$uri$args $uri$args/ /index.html";
          };

          "~ ^(.+\\.php)(.*)$" = {
            root = "/home/$user/public_html/";
            index = "index.php";
            extraConfig = ''
              autoindex on;
              fastcgi_split_path_info ^(.+\.php)(/.+)$;
              fastcgi_pass unix:${config.services.phpfpm.pools.${php_pool}.socket};
              include ${pkgs.nginx}/conf/fastcgi.conf;
            '';
            tryFiles = "$uri$args $uri$args/ /index.php";
          };
        };
      };
    };
  };
}
