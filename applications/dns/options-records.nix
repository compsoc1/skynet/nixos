/*
Define the options for dns records here.
They are imported into anything that needs to use them
*/
{lib, ...}:
with lib; {
  options = {
    domain = lib.mkOption {
      description = "Domain this record is for";
      type = lib.types.str;
      default = "skynet.ie";
    };
    record = lib.mkOption {
      description = "What you want to name the subdomain.";
      type = lib.types.str;
    };
    r_type = lib.mkOption {
      description = "Type of record that this is.";
      type = lib.types.enum ["A" "CNAME" "TXT" "PTR" "SRV" "MX"];
    };
    value = lib.mkOption {
      description = "What the record points to, normally ip or another record.";
      type = lib.types.str;
    };
    server = lib.mkOption {
      description = "Core record for a server";
      type = lib.types.bool;
      default = false;
    };
  };
}
