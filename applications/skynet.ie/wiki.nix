{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  name = "wiki";
  cfg = config.services.skynet."${name}";
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Wiki";
  };

  config = mkIf cfg.enable {
    services.skynet.acme.domains = [
      "renew.skynet.ie"
      "wiki.skynet.ie"
    ];

    services.skynet.dns.records = [
      {
        record = "renew";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
      {
        record = "wiki";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.nginx = {
      virtualHosts = {
        "wiki.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.skynet_website_renew.defaultPackage."x86_64-linux"}";
        };

        # redirect old links to the new wiki
        "renew.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          locations."/".return = "307 https://wiki.skynet.ie";
        };
      };
    };
  };
}
