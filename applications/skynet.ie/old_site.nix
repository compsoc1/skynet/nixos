{year}: {
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; {
  imports = [];

  config = {
    services.skynet.acme.domains = [
      "${year}.skynet.ie"
    ];

    services.skynet.dns.records = [
      {
        record = year;
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.nginx = {
      virtualHosts = {
        "${year}.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs."skynet_website_${year}".defaultPackage."x86_64-linux"}";
        };
      };
    };
  };
}
