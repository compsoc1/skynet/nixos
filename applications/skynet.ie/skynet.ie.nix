{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  name = "website";
  cfg = config.services.skynet."${name}";
in {
  imports = [
    # import in past website versions, available at $year.skynet.ie
    # at teh end of teh year add it here
    (import ./old_site.nix {year = "2023";})
    (import ./old_site.nix {year = "2017";})
    (import ./old_site.nix {year = "2009";})
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Main Website";
  };

  config = mkIf cfg.enable {
    services.skynet.acme.domains = [
      "discord.skynet.ie"
      "public.skynet.ie"
    ];

    services.skynet.dns.records = [
      # means root domain, so skynet.ie
      {
        record = "@";
        r_type = "A";
        value = config.services.skynet.host.ip;
      }
      {
        record = "discord";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
      {
        record = "public";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.nginx = {
      virtualHosts = {
        # main site
        "skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          locations = {
            "/".root = "${inputs.skynet_website.defaultPackage."x86_64-linux"}";

            # this redirects old links to new format
            "~* ~(?<username>[a-z_0-9]*)(?<files>\\S*)$" = {
              priority = 1;
              return = "307 https://$username.users.skynet.ie$files";
            };
          };
        };

        # a custom discord url, because we are too cheap otehrwise
        "discord.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          locations."/".return = "307 https://discord.gg/mkuKJkCuyM";
        };

        "public.skynet.ie" = {
          forceSSL = true;
          useACMEHost = "skynet";
          root = "${inputs.compsoc_public.packages.x86_64-linux.default}";
          locations."/".extraConfig = "autoindex on;";
        };
      };
    };
  };
}
