{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  # root service
  cfg = config.services.skynet;
in {
  imports = [
    # every server needs to have a dns record
    ./dns/dns.nix

    # every server should have proper certs
    ./acme.nix
    ./nginx.nix

    # every server may need the firewall config stuff
    ./firewall.nix

    # every server needs teh ldap client for admins
    ./ldap/client.nix

    # every server will need the config to backup to
    ./restic.nix

    # every server will be monitored for grafana
    ./prometheus.nix
  ];

  options.services.skynet = {
    # since we use this basically everywhere provide a standard way to set it
    host = {
      ip = mkOption {
        type = types.str;
      };
      name = mkOption {
        type = types.str;
      };
      hostname = mkOption {
        type = types.str;
        default = "${cfg.host.name}.skynet.ie";
      };
    };
  };

  config = {
    services.skynet.dns.records = [
      {
        record = cfg.host.name;
        r_type = "A";
        value = cfg.host.ip;
        server = true;
      }
      {
        record = cfg.host.ip;
        r_type = "PTR";
        value = cfg.host.hostname;
      }
    ];

    services.nginx = {
      virtualHosts = {
        # for every server unless explisitly defined redirect the ip to skynet.ie
        "${cfg.host.ip}" = {
          forceSSL = true;
          useACMEHost = "skynet";
          locations."/".return = "307 https://skynet.ie";
        };
      };
    };
  };
}
