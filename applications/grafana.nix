{
  lib,
  config,
  ...
}:
with lib; let
  name = "grafana";
  cfg = config.services.skynet."${name}";
  port = 4444;
in {
  imports = [
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Grafana Server";

    datasource = {
      name = mkOption {
        type = types.str;
      };

      url = mkOption {
        type = types.str;
      };
    };
  };

  config = mkIf cfg.enable {
    services.skynet.dns.records = [
      {
        record = "${name}";
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.skynet.acme.domains = [
      "${name}.skynet.ie"
    ];

    age.secrets.grafana_pw = {
      file = ../secrets/grafana/pw.age;
      owner = "grafana";
      group = "grafana";
    };

    services.grafana = {
      enable = true;
      domain = "${name}.skynet.ie";
      port = port;

      settings.security.admin_password = "$__file{${config.age.secrets.grafana_pw.path}}";

      provision = {
        enable = true;
        datasources.settings.datasources = [
          {
            name = "Prometheus";
            type = "prometheus";
            url = "http://localhost:${toString config.services.skynet.prometheus.server.port}";
            isDefault = true;
            editable = true;
          }
        ];
      };
    };

    services.nginx.virtualHosts = {
      "${name}.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        locations."/" = {
          proxyPass = "http://localhost:${toString port}";
          proxyWebsockets = true;
        };
      };
    };
  };
}
