{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; let
  name = "games";
  cfg = config.services.skynet."${name}";
in {
  imports = [
    ./nginx.nix
    ./games/minecraft.nix
  ];

  options.services.skynet."${name}" = {
    enable = mkEnableOption "Skynet Games";

    domain = {
      tld = mkOption {
        type = types.str;
        default = "ie";
      };

      base = mkOption {
        type = types.str;
        default = "skynet";
      };

      sub = mkOption {
        type = types.str;
        default = "games";
      };
    };
  };

  config = mkIf cfg.enable {
    services.skynet.dns.records = [
      # need a base domain
      {
        record = cfg.domain.sub;
        r_type = "CNAME";
        value = config.services.skynet.host.name;
      }
    ];

    services.skynet.acme.domains = [
      "${cfg.domain.sub}.skynet.ie"
    ];

    services.nginx.virtualHosts = {
      "${cfg.domain.sub}.skynet.ie" = {
        forceSSL = true;
        useACMEHost = "skynet";
        root = "${inputs.skynet_website_games.defaultPackage.x86_64-linux}";
      };
    };

    # the minecraft servers
    services.skynet.games_minecraft = {
      enable = true;

      domain = {
        sub = "minecraft.${cfg.domain.sub}";
      };
    };
  };
}
