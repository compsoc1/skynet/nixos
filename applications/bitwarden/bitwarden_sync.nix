{
  pkgs,
  config,
  lib,
  ...
}: let
  user = "bwdc";
in {
  imports = [
    ./bitwarden-directory-connector-cli.nix
  ];

  options = {};

  config = {
    age.secrets.bitwarden_sync_id = {
      file = ../../secrets/bitwarden/id.age;
      owner = user;
      group = user;
    };
    age.secrets.bitwarden_sync_secret = {
      file = ../../secrets/bitwarden/secret.age;
      owner = user;
      group = user;
    };
    age.secrets.bitwarden_sync_ldap = {
      file = ../../secrets/ldap/pw.age;
      owner = user;
      group = user;
    };

    services.bitwarden-directory-connector-cli = {
      enable = true;

      user = user;

      domain = "https://pw.skynet.ie";

      ldap = {
        ssl = false;
        startTls = false;
        sslAllowUnauthorized = false;
        ad = false;
        port = 389;
        hostname = "account.skynet.ie";
        rootPath = "dc=skynet,dc=ie";
        username = "cn=admin,dc=skynet,dc=ie";
      };

      sync = {
        removeDisabled = true;
        overwriteExisting = false;
        largeImport = false;
        memberAttribute = "member";
        creationDateAttribute = "skCreated";

        users = true;
        userPath = "ou=users";
        userObjectClass = "inetOrgPerson";
        userEmailAttribute = "skMail";
        userFilter = "(|(memberOf=cn=skynet-committee,ou=groups,dc=skynet,dc=ie)(memberOf=cn=skynet-admins,ou=groups,dc=skynet,dc=ie))";

        groups = true;
        groupPath = "ou=groups";
        groupObjectClass = "groupOfNames";
        groupNameAttribute = "cn";
      };

      secrets = {
        ldap = config.age.secrets.bitwarden_sync_ldap.path;
        bitwarden = {
          client_path_id = config.age.secrets.bitwarden_sync_id.path;
          client_path_secret = config.age.secrets.bitwarden_sync_secret.path;
        };
      };
    };
  };
}
