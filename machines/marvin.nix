/*

Name:     https://en.wikipedia.org/wiki/Marvin_the_Paranoid_Android
Why:      Has terrible pain in all the diodes down its left side
Type:     VM
Hardware: -
From:     2023
Role:     For trainees.
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  name = "marvin";
  ip_pub = "193.1.99.81";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };

  groups = [
    "skynet-admins-linux"
    "skynet-trainees-linux"
  ];
  groups_trusted = map (x: "@${x}") groups;
in {
  imports = [
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    # not deployed automatically as its a test server
    tags = [];
  };

  # allow trainees to deploy
  nix.settings.trusted-users =
    [
      "root"
    ]
    ++ groups_trusted;

  # allow trainees access
  services.skynet.ldap_client = {
    groups = groups;
    sudo_groups = groups;
  };

  services.skynet = {
    host = host;
    backup.enable = true;
  };
}
