/*

Name:     https://zim.fandom.com/wiki/GIR
Why:      Gir used to have this role before, servers never die
Type:     VM
Hardware: -
From:     2023
Role:     Email Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "gir";
  ip_pub = "193.1.99.76";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/email.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    email.enable = true;
  };
}
