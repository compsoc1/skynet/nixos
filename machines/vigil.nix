/*

Name:     https://masseffect.fandom.com/wiki/Vigil
Why:      Counterpart to Vendetta
Type:     VM
Hardware: -
From:     2023
Role:     DNS Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  name = "vigil";
  ip_pub = "193.1.99.109";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
  ];

  deployment = {
    targetHost = ip_pub;
    targetPort = 22;
    targetUser = null;

    tags = ["active-dns" "dns"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    dns = {
      server = {
        enable = true;
        # secondary dns server (ns2)
        primary = false;
        ip = ip_pub;
      };
    };
  };
}
