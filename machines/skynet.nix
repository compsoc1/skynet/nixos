/*

Name:     https://en.wikipedia.org/wiki/Skynet_(Terminator)
Why:      Skynet is eternal
Type:     VM
Hardware: -
From:     2023
Role:     Webserver and member linux box
Notes:    Does not host offical sites
*/
{
  pkgs,
  lib,
  nodes,
  inputs,
  ...
}: let
  name = "skynet";
  # DMZ that ITD provided
  ip_pub = "193.1.96.165";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/skynet_users.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    # this one is manually deployed
    tags = ["active-ext"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    website_users.enable = true;
  };
}
