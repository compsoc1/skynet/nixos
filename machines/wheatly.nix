/*

Name:     https://theportalwiki.com/wiki/Wheatley
Why:      Whereever GLaDOS is Wheatly is not too far away
Type:     VM
Hardware: -
From:     2023
Role:     Gitlab Runner
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "wheatly";
  ip_pub = "193.1.99.78";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/gitlab_runner.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-gitlab"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;

    gitlab_runner = {
      enable = true;
      runner.name = "runner01";
    };
  };
}
