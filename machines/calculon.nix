/*

Name:     https://futurama.fandom.com/wiki/Calculon
Why:      Public Service server
Type:     VM
Hardware: -
From:     2024
Role:     Public services such as Nix Cache, Open governance stuff.
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  inputs,
  ...
}: let
  name = "calculon";
  ip_pub = "193.1.99.82";
  hostname = "${name}.skynet.ie";

  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/nix_cache/nix_cache.nix
    ../applications/open_governance/open_governance.nix
    ../applications/open_governance/keyserver.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    nix-cache.enable = true;
    open-governance.enable = true;
    keyserver.enable = true;
  };
}
