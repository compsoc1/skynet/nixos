/*

Name:     https://williamgibson.fandom.com/wiki/Neuromancer_(AI)
Why:      A sibling to Wintermute, stores and archives memories.
Type:     VM
Hardware: -
From:     2023
Role:     Backup Server
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "neuromancer";
  ip_pub = "193.1.99.80";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ./hardware/RM007.nix
  ];

  networking.hostName = name;
  # this has to be defined for any physical servers
  # vms are defined by teh vm host
  networking = {
    defaultGateway.interface = lib.mkForce "eno1";
    interfaces.eno1.ipv4.addresses = [
      {
        address = ip_pub;
        prefixLength = 26;
      }
    ];
  };

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  services.skynet = {
    host = host;
    backup.server.enable = true;
  };
}
