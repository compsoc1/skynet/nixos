{
  pkgs,
  modulesPath,
  config,
  options,
  inputs,
  lib,
  ...
}:
with lib; let
  cfg = config.skynet;
in {
  imports = [
    # custom lxc mocule until the patch gets merged in
    ../applications/proxmox-lxc.nix
    # (modulesPath + "/virtualisation/proxmox-lxc.nix")

    # for the secrets
    inputs.agenix.nixosModules.default

    # base application config for all servers
    ../applications/_base.nix
  ];

  options.skynet = {
    lxc = mkOption {
      type = types.bool;
      # most of our servers are lxc so its true by default
      default = true;
      description = mdDoc "Is this a Linux Container?";
    };
  };

  config = {
    # if its a lxc enable
    proxmoxLXC.enable = cfg.lxc;

    nix = {
      settings = {
        # flakes are essensial
        experimental-features = ["nix-command" "flakes"];
        trusted-users = [
          "root"
          "@skynet-admins-linux"
        ];
      };

      # https://nixos.wiki/wiki/Storage_optimization
      #      gc = {
      #        automatic = true;
      #        dates = "weekly";
      #        options = "--delete-older-than 30d";
      #      };

      # to free up to 10GiB whenever there is less than 1GiB left
      extraOptions = ''
        min-free = ${toString (1024 * 1024 * 1024)}
        max-free = ${toString (1024 * 1024 * 1024 * 10)}
      '';
    };

    system.stateVersion = "22.11";

    services.openssh = {
      enable = true;
      settings.PermitRootLogin = "prohibit-password";
    };

    users.users.root = {
      initialHashedPassword = "";

      openssh.authorizedKeys.keys = [
        # no obligation to have name attached to keys

        # Root account
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK6DjXTAxesXpQ65l659iAjzEb6VpRaWKSg4AXxifPw9 Skynet Admin"

        # CI/CD key
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBDvexq/JjsMqL0G5P38klzoOkHs3IRyXYO1luEJuB5R colmena_key"

        # Brendan Golden
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEHNLroAjCVR9Tx382cqdxPZ5KY32r/yoQH1mgsYNqpm Silver_Laptop_WSL_Deb"

        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKjaKI97NY7bki07kxAvo95196NXCaMvI1Dx7dMW05Q1 thenobrainer"
      ];
    };

    # skynet-admin-linux will always be added, individual servers can override the groups option
    services.skynet.ldap_client.enable = true;

    networking = {
      # every sever needs to be accessable over ssh for admin use at least
      firewall.allowedTCPPorts = [22];

      # explisitly stating this is good
      defaultGateway = {
        address = "193.1.99.65";
        interface = "eth0";
      };

      # cannot use our own it seems?
      nameservers = [
        # ns1
        "193.1.99.120"
        # ns2
        "193.1.99.109"
      ];
    };

    # time on vendetta is strangely out of sync
    networking.timeServers = options.networking.timeServers.default ++ ["ie.pool.ntp.org"];
    services.ntp.enable = true;

    # use teh above nameservers as the fallback dns
    services.resolved.fallbackDns = config.networking.nameservers;

    # https://discourse.nixos.org/t/systemd-networkd-wait-online-934764-timeout-occurred-while-waiting-for-network-connectivity/33656/9
    systemd.network.wait-online.enable = false;

    environment.systemPackages = [
      # for flakes
      pkgs.git
      # useful tools
      pkgs.ncdu_2
      pkgs.htop
      pkgs.nano
      pkgs.nmap
      pkgs.bind
      pkgs.zip
      pkgs.traceroute
      pkgs.openldap
      pkgs.screen
    ];
  };
}
