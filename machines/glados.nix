/*

Name:     https://half-life.fandom.com/wiki/GLaDOS
Why:      Glados has a vast experence of testing and deploying.
Type:     VM
Hardware: -
From:     2023
Role:     Git server
Notes:    Each user has roughly 20gb os storage
          20 * 100 = 2000gb
*/
{
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "glados";
  ip_pub = "193.1.99.75";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/gitlab.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-gitlab"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    gitlab.enable = true;
  };
}
