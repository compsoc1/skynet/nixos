/*

Name:     https://en.wikipedia.org/wiki/KITT
Why:      Kitt used to have this role before (as well as email and dns)
Type:     VM
Hardware: -
From:     2023
Role:     LDAP Server
Notes:
*/
{
  config,
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "kitt";
  ip_pub = "193.1.99.74";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/ldap/server.nix
    ../applications/ldap/backend.nix
    ../applications/discord.nix
    ../applications/bitwarden/vaultwarden.nix
    ../applications/bitwarden/bitwarden_sync.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;

    # ldap setup
    ldap.enable = true;
    ldap_backend.enable = true;

    # private member services
    discord_bot.enable = true;

    # committee/admin services
    vaultwarden.enable = true;
  };
}
