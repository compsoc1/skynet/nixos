/*

Name:     https://hitchhikers.fandom.com/wiki/Earth
Why:      Our home(page)
Type:     VM
Hardware: -
From:     2023
Role:     Webserver
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  inputs,
  ...
}: let
  name = "earth";
  ip_pub = "193.1.99.79";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/skynet.ie/skynet.ie.nix
    ../applications/skynet.ie/wiki.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    website.enable = true;
    wiki.enable = true;
  };
}
