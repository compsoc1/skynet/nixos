/*

Name:     https://en.wikipedia.org/wiki/Optimus_Prime
Why:      Created to sell toys so this vm is for games
Type:     VM
Hardware: -
From:     2023
Role:     Game host
Notes:
*/
{
  pkgs,
  lib,
  nodes,
  arion,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "optimus";
  ip_pub = "193.1.99.112";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/games.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;
    games.enable = true;
  };
}
