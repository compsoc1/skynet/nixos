/*

Name:     https://en.wikipedia.org/wiki/Eagle_Eye
Why:      ARIIA - Autonomous Reconnaissance Intelligence Integration Analyst
Type:     VM
Hardware: -
From:     2024
Role:     Metrics gathering and Analysis
Notes:
*/
{
  config,
  pkgs,
  lib,
  nodes,
  ...
}: let
  # name of the server, sets teh hostname and record for it
  name = "ariia";
  ip_pub = "193.1.99.83";
  hostname = "${name}.skynet.ie";
  host = {
    ip = ip_pub;
    name = name;
    hostname = hostname;
  };
in {
  imports = [
    ../applications/grafana.nix
  ];

  deployment = {
    targetHost = hostname;
    targetPort = 22;
    targetUser = null;

    tags = ["active-core"];
  };

  services.skynet = {
    host = host;
    backup.enable = true;

    prometheus.server.enable = true;
    grafana.enable = true;
  };
}
