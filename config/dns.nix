{lib, ...}: {
  imports = [
  ];

  options.skynet.records = lib.mkOption {
    description = "Records, sorted based on therir type";
    type = lib.types.listOf (lib.types.submodule (import ../applications/dns/options-records.nix {
      inherit lib;
    }));
  };

  config = {
    skynet.records =
      [
        {
          record = "optimus";
          r_type = "A";
          value = "193.1.99.90";
          server = true;
        }
        {
          record = "panel.games";
          r_type = "CNAME";
          value = "optimus";
        }
        {
          record = "bumblebee";
          r_type = "A";
          value = "193.1.99.91";
          server = true;
        }
        {
          record = "minecraft.compsoc.games";
          r_type = "CNAME";
          value = "bumblebee";
        }
        {
          record = "_minecraft._tcp.minecraft.compsoc.games.skynet.ie.";
          r_type = "SRV";
          value = "0 10 25518 bumblebee.skynet.ie.";
        }
        {
          record = "minecraft-classic.compsoc.games";
          r_type = "CNAME";
          value = "bumblebee";
        }
        {
          record = "_minecraft._tcp.minecraft-classic.compsoc.games.skynet.ie.";
          r_type = "SRV";
          value = "0 10 25518 bumblebee.skynet.ie.";
        }
        {
          record = "minecraft.gsoc.games";
          r_type = "CNAME";
          value = "bumblebee";
        }
        {
          record = "_minecraft._tcp.minecraft.gsoc.games.skynet.ie.";
          r_type = "SRV";
          value = "0 10 25521 bumblebee.skynet.ie.";
        }
        {
          record = "minecraft.phildeb.games";
          r_type = "CNAME";
          value = "bumblebee";
        }
        {
          record = "_minecraft._tcp.minecraft.phildeb.games.skynet.ie.";
          r_type = "SRV";
          value = "0 10 25522 bumblebee.skynet.ie.";
        }
        {
          record = "minecraft-aged.compsoc.games";
          r_type = "CNAME";
          value = "bumblebee";
        }
        {
          record = "_minecraft._tcp.minecraft-aged.compsoc.games.skynet.ie.";
          r_type = "SRV";
          value = "0 10 25519 bumblebee.skynet.ie.";
        }
      ]
      # non skynet domains
      ++ [
        {
          domain = "conradcollins.net";
          record = "www";
          r_type = "CNAME";
          value = "skynet.skynet.ie.";
        }

        {
          domain = "edelharty.net";
          record = "www";
          r_type = "CNAME";
          value = "skynet.skynet.ie.";
        }
        {
          domain = "damienconroy.com";
          record = "www";
          r_type = "CNAME";
          value = "skynet.skynet.ie.";
        }
      ];
  };
}
