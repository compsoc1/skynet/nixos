# borrowed from https://gitlab.com/nix17/nixos-config/-/blob/main/.gitlab-ci.yml

stages:
  - misc
  - test
  - deploy
  - deploy_gitlab

# Update the flake for any changes upstream
# Passed in from upstream
# $PACKAGE_NAME = name of the flake that needs to be updated
# $UPDATE_FLAKE = flag to update the flake
update:
  stage: misc
  tags:
    - nix
  # from https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554/5
  before_script:
    # set teh ssh key for the commit
    - eval $(ssh-agent -s)
    - echo "${CI_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$CI_KEY" >> ~/.ssh/id_rsa.pub
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

  script:
    - git config --global user.email "${CI_EMAIL}"
    - git config --global user.name "${CI_USERNAME}"
    # the part that updates the flake
    - nix --experimental-features 'nix-command flakes' flake lock --update-input $PACKAGE_NAME
    - git add flake.lock
    - git commit -m "Updated flake for $PACKAGE_NAME" || echo "No changes, nothing to commit"
    # we have a custom domain
    - git remote rm origin && git remote add origin ssh://git@gitlab.skynet.ie:2222/compsoc1/skynet/nixos.git
    - git push origin HEAD:$CI_COMMIT_REF_NAME
  only:
    refs:
      - main
    variables:
      - $UPDATE_FLAKE == "yes"

sync_repos:
  stage: misc
  image: registry.gitlab.com/gitlab-ci-utils/curl-jq:2.0.0
  script:
    - cd sync
    - chmod +x ./sync.sh
    - ./sync.sh
  rules:
    - if: $UPDATE_FLAKE == "yes"
      when: never
    - if: '$CI_PROJECT_NAMESPACE == "compsoc1/skynet" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - sync/repos.csv

.scripts_base: &scripts_base
  # load nix environment
  - . "$HOME/.nix-profile/etc/profile.d/nix.sh"
  - nix --extra-experimental-features 'nix-command flakes'  profile install nixpkgs#colmena

.scripts_deploy: &scripts_deploy
  # setup ssh key
  - eval $(ssh-agent -s)
  - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh

.scripts_cache: &scripts_cache
  - nix --extra-experimental-features 'nix-command flakes'  profile install nixpkgs#attic-client
  - attic login skynet https://nix-cache.skynet.ie/ $CACHE_KEY
  - attic use skynet-cache
  # add any new items to the cache
  - attic watch-store skynet-cache &

# every commit on main will build and deploy
.build_template: &builder
  tags:
    - nix
  before_script:
    - *scripts_base
    - *scripts_cache
  rules:
    - if: $UPDATE_FLAKE == "yes"
      when: never
    - changes:
      - applications/**/*
      - machines/**/*
      - secrets/**/*
      - flake.*
      - .gitlab-ci.yml
      - config/**/*

# deploy items only run on main
.deploy_template: &deployment
  before_script:
    - *scripts_deploy
    - *scripts_base
    - *scripts_cache
  rules:
    - if: $UPDATE_FLAKE == "yes"
      when: never
    - if: '$CI_PROJECT_NAMESPACE == "compsoc1/skynet" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - flake.nix
        - flake.lock
        - applications/**/*
        - machines/**/*
        - secrets/**/*
        - config/**/*

linter:
  <<: *builder
  stage: test
  script:
    - nix --extra-experimental-features 'nix-command flakes' fmt -- --check .

build:
  <<: *builder
  stage: test
  script:
    - nix --extra-experimental-features 'nix-command flakes' develop
    - colmena build -v --on @active-dns
    - colmena build -v --on @active-core
    - colmena build -v --on @active
    - colmena build -v --on @active-ext
    - colmena build -v --on @active-gitlab

# dns always has to be deployed first
deploy_dns:
  <<: *builder
  <<: *deployment
  stage: deploy
  script:
    - colmena apply -v --on @active-dns

deploy_core:
  <<: *builder
  <<: *deployment
  stage: deploy
  needs:
    - deploy_dns
  script:
    - colmena apply -v --on @active-core

deploy_active:
  <<: *builder
  <<: *deployment
  stage: deploy
  needs:
    - deploy_dns
  script:
    - colmena apply -v --on @active

# this is just skynet server
deploy_ext:
  <<: *builder
  <<: *deployment
  stage: deploy
  needs:
    - deploy_dns
  script:
    - colmena apply -v --on @active-ext

deploy_gitlab:
  <<: *builder
  <<: *deployment
  stage: deploy_gitlab
  script:
    - colmena apply -v --on @active-gitlab
  when: manual
